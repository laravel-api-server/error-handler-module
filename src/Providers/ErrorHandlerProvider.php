<?php

namespace ApiServer\ErrorHandler\Providers;

use App;
use Illuminate\Support\ServiceProvider;

use ApiServer\ErrorHandler\Exceptions\ExceptionHandlerManager;

class ErrorHandlerProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        App::singleton(ExceptionHandlerManager::class, function () {
            return new ExceptionHandlerManager;
        });
    }
}
