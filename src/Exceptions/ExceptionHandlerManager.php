<?php

namespace ApiServer\ErrorHandler\Exceptions;

use Exception;
use Illuminate\Http\Request;
use \Symfony\Component\HttpFoundation\Response;

use ApiServer\ErrorHandler\Contracts\ExceptionHandler;
use ApiServer\ErrorHandler\Exceptions\Handler\FallbackExceptionHandler;

class ExceptionHandlerManager {
    protected $handlers = [];

    /**
     * Handles an exception in the context of provided request
     *
     * @param \Exception $e
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \RuntimeException
     */
    public function handle(Exception $e, Request $request): Response {
        \Log::info($this->handlers);
        foreach($this->handlers as $handler) {
            if($handler->manages($e, $request))
                return $handler->handle($e, $request);
        }

        return (new FallbackExceptionHandler)->handle($e, $request);
    }

    /**
     * Register a new exception handler.
     *
     * @param \ApiServer\ErrorHandler\Contracts\ExceptionHandler $handler
     * @param boolean $append
     * @return void
     */
    public function registerHandler(
        ExceptionHandler $handler,
        bool $append = false
    ) {
        if(!$append) {
            array_unshift($this->handlers, $handler);
        } else {
            $this->handlers[] = $handler;
        }
    }
}
