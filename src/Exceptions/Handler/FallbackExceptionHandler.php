<?php

namespace ApiServer\ErrorHandler\Exceptions\Handler;

use Exception;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Routing\Route;
use Illuminate\Foundation\Exceptions\Handler as LaravelExceptionHandler;

use ApiServer\ErrorHandler\Contracts\ExceptionHandler;

class FallbackExceptionHandler extends ExceptionHandler
{
    protected function managesRoute(Route $route): bool {
        return true;
    }

    protected function managesException(Exception $e): bool {
        return true;
    }

    public function handle(Exception $e, Request $request): Response {
        $laravelHandler = app(LaravelExceptionHandler::class);
        return $laravelHandler->render($request, $e);
    }
}
