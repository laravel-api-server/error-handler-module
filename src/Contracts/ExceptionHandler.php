<?php

namespace ApiServer\ErrorHandler\Contracts;

use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Routing\Route;

abstract class ExceptionHandler {
    protected function managesRoute(Route $route): bool
    {
        return true;
    }

    abstract protected function managesException(Exception $e): bool;

    /**
     * If the exception handler is able to format a response for the provided exception,
     * then the implementation should return true.
     *
     * @param \Exception $e
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function manages(Exception $e, Request $request): bool {
        return ($this->managesException($e)
                && (is_null($request->route())
                    || $this->managesRoute($request->route()))
        );
    }

    /**
     * Handle the provided exception.
     *
     * @param \Exception $e
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    abstract public function handle(Exception $e, Request $request): Response;
}
