# About this package
This package provides advanced errorhandling for Laravel.

# Using this package
Register service provider in `config/app.php` by adding the following line to
the `providers`-array:
```
        ApiServer\ErrorHandler\Providers\ModuleServiceProvider::class,
```

Adjust your `render()`-method in `àpp/Exceptions/Handler.php` to become:
```
public function render($request, Exception $exception)
{
    $errorHandler = \App::make(ErrorHandler::class);
    return $errorHandler->handle($exception, $request);
}
```

Next create a directory structure that looks like this:
```
app/Exceptions/Handler/<ResponseType>/
app/Exceptions/Handler/<WebResponseInExample>/
app/Exceptions/Handler/<JsonApiResponseInExample>/
```

Inside these directories you create your exception handlers:
```
<?php

namespace Netmon\Devices\Exceptions\Handler\WebResponse;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use ApiServer\ErrorHandler\Contracts\ExceptionHandlerInterface;
use Netmon\Devices\Exceptions\Exceptions\ErrorException;

class ErrorExceptionHandler implements ExceptionHandlerInterface
{
    public function manages(Exception $e, Request $request): bool {
        return ($e instanceof ErrorException);
    }

    public function handle(Exception $e, Request $request): Response {
        $status = 200;
        $message = "This is an error example"

        return response($message, $status);
    }
}
```

You can register the error handler by creating a service provider with
a `register()`-method:
```
public function register()
{
    // get ErrorHandler singleton
    $errorHandler = \App::make(ErrorHandler::class);

    // register custom error handlers
    $errorHandler->registerHandler(new ErrorExceptionHandler);
}
```

If you want to make your Handler apply to just some special named routes only,
you can pass the matching routes like this during registration:
```
    $errorHandler->registerHandler(new ErrorExceptionHandler, ['api.', 'web.']);
```

If you want to make shure that you handler gets checked after all other handlers
have been checked you can append the handler by adding `true` as third
parameter (designed for registering fallback handlers in a modularized environment):
```
    $errorHandler->registerHandler(new ErrorExceptionHandler, [], true);
```

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

## Mailinglist
https://lists.ffnw.de/mailman/listinfo/netmon-dev

# Credits
Ideas where thankfully token from the
[tobscure/json-api](https://github.com/tobscure/json-api) library.

# License
See [License](LICENSE.txt)
